#!/bin/sh -e

VERSION=$2
TAR=../maven-plugin-testing_$VERSION.orig.tar.xz
DIR=maven-plugin-testing-$VERSION
TAG=$(echo "maven-plugin-testing-$VERSION" | sed -re's/~(alpha|beta)/-\1-/')

svn export http://svn.apache.org/repos/asf/maven/plugin-testing/tags/${TAG}/ $DIR
XZ_OPT=--best tar -c -J -f $TAR --exclude '*.jar' --exclude '*.class' $DIR
rm -rf $DIR ../$TAG

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir && echo "moved $TAR to $origDir"
fi
